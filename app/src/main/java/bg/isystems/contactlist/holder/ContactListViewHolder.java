package bg.isystems.contactlist.holder;

import android.os.Bundle;
import android.view.View;

import java.sql.SQLException;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import bg.isystems.contactlist.databinding.ContactRecyclerviewBinding;
import bg.isystems.contactlist.model.Contact;

public class ContactListViewHolder extends RecyclerView.ViewHolder{

    private ContactRecyclerviewBinding binding;

    public ContactListViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public ContactListViewHolder(ContactRecyclerviewBinding binding){
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bindTo(Contact contact)
    {
        binding.setData(contact);
    }

    public void clear() {
        binding.invalidateAll();
    }
}
