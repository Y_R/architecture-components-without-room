package bg.isystems.contactlist.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.query.In;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.SQLException;
import java.util.concurrent.Callable;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.PropertyChangeRegistry;
import androidx.recyclerview.widget.DiffUtil;
import bg.isystems.contactlist.BR;

@DatabaseTable
public class Contact extends BaseDaoEnabled<Contact, Integer> implements Observable {

    private final PropertyChangeRegistry registry = new PropertyChangeRegistry();

    @DatabaseField(columnName = "id", generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @DatabaseField
    private String lastName;

    @DatabaseField
    private String number;

    public Contact(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(this.name == null || !this.name.equals(name)){
            this.name = name;
            registry.notifyChange(this,BR.name);
        }
    }

    @Bindable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if(this.lastName == null || !this.lastName.equals(lastName)){
            this.lastName = lastName;
            registry.notifyChange(this, BR.lastName);
        }

    }

    @Bindable
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        if(this.number == null || !this.number.equals(number)){
            this.number = number;
            registry.notifyChange(this,BR.number);
        }

    }

    @Override
    public void addOnPropertyChangedCallback(Observable.OnPropertyChangedCallback onPropertyChangedCallback) {
        registry.add(onPropertyChangedCallback);
    }

    @Override
    public void removeOnPropertyChangedCallback(Observable.OnPropertyChangedCallback onPropertyChangedCallback) {
        registry.remove(onPropertyChangedCallback);
    }


    public static final DiffUtil.ItemCallback<Contact> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Contact>() {
                @Override
                public boolean areItemsTheSame(@NonNull Contact oldContact, @NonNull Contact newContact) {
                    return oldContact.getId() == newContact.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull Contact oldContact, @NonNull Contact newContact) {
                    return oldContact.equals(newContact);
                }
            };
}
