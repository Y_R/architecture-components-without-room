package bg.isystems.contactlist.ui;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Objects;

import androidx.paging.DataSource;
import androidx.paging.PagedList;
import bg.isystems.contactlist.data.DatabaseHelper;
import bg.isystems.contactlist.paging.PagingHelper;
import bg.isystems.contactlist.viewmodel.MainViewModel;
import bg.isystems.contactlist.R;
import bg.isystems.contactlist.databinding.NewContactFragmentBinding;
import bg.isystems.contactlist.model.Contact;
import bg.isystems.contactlist.model.OnClickListener;
import bg.isystems.contactlist.viewmodel.ViewModelFactory;

public class NewContactFragment extends Fragment implements OnClickListener {

    //private MainViewModel mViewModel;
    NewContactFragmentBinding binding;
    PagingHelper pagingHelper;
    Contact contact;
    public static NewContactFragment newInstance() {
        return new NewContactFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.new_contact_fragment, container, false);
        binding.setClick(this);
        binding.setData(new Contact());
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pagingHelper = PagingHelper.getInstance();
    }

    @Override
    public void onClick() {
        final Contact contact = binding.getData();
        try {
            Dao dao = DatabaseHelper.getInstance(null).getContactDao();
            dao.create(contact);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PagedList<Contact> pagedList = pagingHelper.getPagedListContactLiveData().getValue();
        if(pagedList != null)
            pagedList.getDataSource().invalidate();
        binding.setData(new Contact());
    }
}
