package bg.isystems.contactlist.ui;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import bg.isystems.contactlist.adapter.ContactListPagedAdapter;
import bg.isystems.contactlist.data.DatabaseHelper;
import bg.isystems.contactlist.model.Contact;
import bg.isystems.contactlist.paging.PagingHelper;
import bg.isystems.contactlist.viewmodel.MainViewModel;
import bg.isystems.contactlist.R;
import bg.isystems.contactlist.viewmodel.ViewModelFactory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Objects;

public class ListContactFragment extends Fragment {

    View view;
    private PagingHelper pagingHelper;
    RecyclerView recyclerView;
    ContactListPagedAdapter adapter;
    Button button;
    public static ListContactFragment newInstance() {
        return new ListContactFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.list_contact_fragment, container, false);

        button = view.findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    final Contact contact = new Contact();
                    contact.setName("Name 1");
                    contact.setLastName("Last Name 1");
                    contact.setNumber("Number 1");
                    try {
                        Dao dao = DatabaseHelper.getInstance(null).getContactDao();
                        dao.create(contact);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    Objects.requireNonNull(pagingHelper.getPagedListContactLiveData().getValue()).getDataSource().invalidate();

                }
            }
        });

        adapter = new ContactListPagedAdapter(view.getContext());
        recyclerView = view.findViewById(R.id.contact_list_recyclerview);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pagingHelper = PagingHelper.getInstance();
        pagingHelper.getPagedListContactLiveData().observe(this, new Observer<PagedList<Contact>>() {
            @Override
            public void onChanged(PagedList<Contact> contacts) {
                adapter.submitList(contacts);
                adapter.onCurrentListChanged(contacts);
            }
        });
    }

}
