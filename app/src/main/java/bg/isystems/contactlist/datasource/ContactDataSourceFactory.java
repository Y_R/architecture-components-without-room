package bg.isystems.contactlist.datasource;

import bg.isystems.contactlist.model.Contact;

public class ContactDataSourceFactory extends DataSourceFactoryAbstract<Contact>{


    @Override
    protected ContactDataSource getDataSource() {
        return new ContactDataSource();
    }

}