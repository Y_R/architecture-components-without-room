package bg.isystems.contactlist.datasource;

import java.sql.SQLException;
import java.util.List;

import bg.isystems.contactlist.data.DatabaseHelper;
import bg.isystems.contactlist.model.Contact;

public class ContactDataSource extends LimitOffsetDataSource<Contact> {//PositionalDataSource<Counting> {


    public ContactDataSource(){

    }

    @Override
    protected long countItems(){
        long count = 0;
        try {
            count = DatabaseHelper.getInstance(null).getContactDao().queryBuilder().countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    protected List<Contact> loadRangeLimitOffset(long startPosition, long loadCount) {

        List<Contact> countings = null;

        try {
            countings = DatabaseHelper.getInstance(null).getContactDao().queryBuilder()
                    .limit(loadCount)
                    .offset(startPosition)
                    .orderBy("id",true)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return countings;
    }
}
