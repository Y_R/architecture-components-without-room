package bg.isystems.contactlist.datasource;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

public abstract class DataSourceFactoryAbstract<T> extends DataSource.Factory<Integer, T> {

    private MutableLiveData<LimitOffsetDataSource<T>> mutableLiveData;

    protected abstract LimitOffsetDataSource<T> getDataSource();

    public DataSourceFactoryAbstract() {
        this.mutableLiveData = new MutableLiveData<>();
    }

    @Override
    public DataSource<Integer, T> create() {

        LimitOffsetDataSource<T> dataSource = getDataSource();

        mutableLiveData.postValue(dataSource);

        return (DataSource<Integer, T>) dataSource;
    }

    public MutableLiveData<LimitOffsetDataSource<T>> getMutableLiveData() {
        return mutableLiveData;
    }

}
