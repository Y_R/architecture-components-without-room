package bg.isystems.contactlist.datasource;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.PositionalDataSource;

public abstract class LimitOffsetDataSource<T> extends PositionalDataSource<T> {

    abstract protected long countItems();
    abstract protected List<T> loadRangeLimitOffset(long position, long size);

    @Override
    public void loadInitial(@NonNull LoadInitialParams params,
                            @NonNull LoadInitialCallback<T> callback) {
        long totalCount = 0;
        totalCount = this.countItems();
        if (totalCount == 0) {
            callback.onResult(Collections.<T>emptyList(), 0, 0);
            return;
        }
        // bound the size requested, based on known count

        int firstLoadPosition = computeInitialLoadPosition(params, (int) totalCount);
        final int firstLoadSize = computeInitialLoadSize(params, firstLoadPosition, (int) totalCount);

//        if(firstLoadPosition == 0){
//            firstLoadPosition = (int) (totalCount - 1);
//        }
        List<T> list = loadRangeLimitOffset(firstLoadPosition, firstLoadSize);
        if (list != null && list.size() == firstLoadSize) {
            //callback.onResult(list, firstLoadPosition, (int) totalCount);
            callback.onResult(list, firstLoadPosition);
        }
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<T> callback) {
        List<T> list = loadRangeLimitOffset(params.startPosition, params.loadSize);
        if (list != null) {
            callback.onResult(list);
        } else {
            invalidate();
        }
    }
}