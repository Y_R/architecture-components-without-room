package bg.isystems.contactlist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.sql.SQLException;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import bg.isystems.contactlist.data.DatabaseHelper;
import bg.isystems.contactlist.model.Contact;
import bg.isystems.contactlist.ui.ListContactFragment;
import bg.isystems.contactlist.ui.NewContactFragment;
import bg.isystems.contactlist.utility.BottomNavigationViewHelper;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    NewContactFragment newContactFragment = null;
    ListContactFragment listContactFragment = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        BottomNavigationViewHelper.removeShiftMode(navigation);
        deleteDatabase(DatabaseHelper.DATABASE_NAME);
        runFragment(new NewContactFragment(),false);

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.navigation_add:
                if(newContactFragment == null)
                    newContactFragment = new NewContactFragment();
                runFragment(newContactFragment,false);
                return true;
            case R.id.navigation_list:
                if(listContactFragment == null)
                    listContactFragment = new ListContactFragment();
                runFragment(listContactFragment, false);
                return true;
        }
        return false;
    }

    public void runFragment(Fragment fragment, boolean addToBackStack){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        //fabVisiblity(fragment);
        if(addToBackStack){
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }
}
