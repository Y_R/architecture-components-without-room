package bg.isystems.contactlist.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import bg.isystems.contactlist.model.Contact;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) MainViewModel.getInstance();
    }
}
