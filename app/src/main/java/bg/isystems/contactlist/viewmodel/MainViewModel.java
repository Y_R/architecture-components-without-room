package bg.isystems.contactlist.viewmodel;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import bg.isystems.contactlist.data.DatabaseHelper;
import bg.isystems.contactlist.datasource.ContactDataSourceFactory;
import bg.isystems.contactlist.datasource.LimitOffsetDataSource;
import bg.isystems.contactlist.model.Contact;

public class MainViewModel extends ViewModel {

    private MutableLiveData<Contact> contactMutableLiveData = new MutableLiveData<>();

    private LiveData<PagedList<Contact>> pagedListContactLiveData;

    private static MainViewModel instance = null;

    public static MainViewModel getInstance(){
        if(instance == null)
            instance = new MainViewModel();

        return instance;
    }

    private MainViewModel() {
        Executor executor = Executors.newFixedThreadPool(10);
        ContactDataSourceFactory dataSourceFactory = new ContactDataSourceFactory();
        LiveData<LimitOffsetDataSource<Contact>> tDataSource = dataSourceFactory.getMutableLiveData();

        PagedList.Config pagedListConfig = (new PagedList.Config.Builder())
                .setEnablePlaceholders(false)
                .setPageSize(20)
                .setPrefetchDistance(5)
                .build();


        pagedListContactLiveData = new LivePagedListBuilder<>(dataSourceFactory, pagedListConfig)
                .setFetchExecutor(executor)
                //.setInitialLoadKey(5)
                .build();

    }
    public LiveData<PagedList<Contact>> getPagedListContactLiveData(){
        return pagedListContactLiveData;
    }

    public void setContact(final Contact contact){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Dao dao = DatabaseHelper.getInstance(null).getContactDao();
                    dao.create(contact);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        contactMutableLiveData.setValue(contact);
    }

    public LiveData<Contact> getContact(){
        return contactMutableLiveData;
    }
}
