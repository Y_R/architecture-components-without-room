package bg.isystems.contactlist;

import android.app.Application;

import bg.isystems.contactlist.data.DatabaseHelper;

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseHelper.getInstance(getApplicationContext());
    }

    public BaseApplication(){
        super();

    }
}
