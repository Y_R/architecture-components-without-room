package bg.isystems.contactlist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedListAdapter;
import bg.isystems.contactlist.R;
import bg.isystems.contactlist.databinding.ContactRecyclerviewBinding;
import bg.isystems.contactlist.holder.ContactListViewHolder;
import bg.isystems.contactlist.model.Contact;
public class ContactListPagedAdapter extends PagedListAdapter<Contact, ContactListViewHolder> {

    private Context context;
    ContactListViewHolder contactListViewHolder;

    public ContactListPagedAdapter(Context context){
        super(Contact.DIFF_CALLBACK);
        this.context = context;
    }

    @Override
    public ContactListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ContactRecyclerviewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.contact_recyclerview,parent,false);
        contactListViewHolder = new ContactListViewHolder(binding);
        return contactListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactListViewHolder holder, int position) {
        Contact contact = getItem(position);
        if (contact != null) {
            holder.bindTo(contact);
        } else {
            holder.clear();
        }
    }
}