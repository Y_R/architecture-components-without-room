package bg.isystems.contactlist.paging;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import bg.isystems.contactlist.data.DatabaseHelper;
import bg.isystems.contactlist.datasource.ContactDataSourceFactory;
import bg.isystems.contactlist.datasource.LimitOffsetDataSource;
import bg.isystems.contactlist.model.Contact;
import bg.isystems.contactlist.viewmodel.MainViewModel;

public class PagingHelper {

    private static PagingHelper instance = null;

    public static PagingHelper getInstance(){
        if(instance == null)
            instance = new PagingHelper();

        return instance;
    }

    private LiveData<PagedList<Contact>> pagedListContactLiveData;

    public PagingHelper() {
        Executor executor = Executors.newFixedThreadPool(10);
        ContactDataSourceFactory dataSourceFactory = new ContactDataSourceFactory();
        LiveData<LimitOffsetDataSource<Contact>> tDataSource = dataSourceFactory.getMutableLiveData();

        PagedList.Config pagedListConfig = (new PagedList.Config.Builder())
                .setEnablePlaceholders(false)
                .setPageSize(20)
                .setPrefetchDistance(5)
                .build();


        pagedListContactLiveData = new LivePagedListBuilder<>(dataSourceFactory, pagedListConfig)
                .setFetchExecutor(executor)
                //.setInitialLoadKey(5)
                .build();

    }
    public LiveData<PagedList<Contact>> getPagedListContactLiveData(){
        return pagedListContactLiveData;
    }

}
